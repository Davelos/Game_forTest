using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Respawn : MonoBehaviour
{
    // Respawn Settings
    [Header("RespawnSettings")]
    [SerializeField]
    private GameObject[] enemysPrefab;
    [SerializeField]
    private Transform[] positionRespawn;
    [SerializeField]
    public float respawnTime;
    [SerializeField]
    private GameObject respawnEffect;
    [SerializeField]
    private Transform targetMove;
    public Transform TargetMove
    {
        get { return targetMove; }
    }

    //GameManager
    [Header("GameManagerScript")]
    [SerializeField]
    private GameManager gameManager;
    public GameManager GameManager
    {
        get { return gameManager; }
        set { gameManager = value; }
    }
    private bool isFreze;

    //list object for EnemyRespawn
    private List<GameObject> countEnemy = new List<GameObject>(); 
    public List<GameObject> CountEnemy
    {
        get { return countEnemy; }
        set { countEnemy = value; }
    }
    
    private void Start()
    {
        if (PlayerPrefs.HasKey("RespawnTime"))
        {
            respawnTime = PlayerPrefs.GetFloat("RespawnTime"); 

            if (respawnTime <= 0.8f)
            {
                respawnTime = 0.8f;
            }
        }

        StartCoroutine(respawnEnemys());
    }
    private IEnumerator respawnEnemys() //RespawnEnemy
    {
        while (gameManager.IsGame && countEnemy.Count < 10 && !gameManager.IsPaused && !isFreze)
        {
            GameObject enemy = Instantiate(enemysPrefab[Random.Range(0, enemysPrefab.Length)], positionRespawn[Random.Range(0, positionRespawn.Length)].position, Quaternion.Euler(0,180,0));
            enemy.transform.parent = transform;
            Instantiate(respawnEffect, enemy.transform.position, Quaternion.identity);

            countEnemy.Add(enemy);
            yield return new WaitForSeconds(respawnTime);
        }

        if (countEnemy.Count >= 10)
        {
            gameManager.Lose();
        } 

        if (gameManager.IsPaused || isFreze)
        {
            while (gameManager.IsPaused || isFreze)
            {
                yield return null;
            }

            StartCoroutine(respawnEnemys());
            yield break;
        }

        countEnemy.Clear();
        yield break;
    }

    public IEnumerator frezeBuster(float timeFreze)
    {
        isFreze = true;
        
        for (int x = 0; x < countEnemy.Count; x++)
        {
            countEnemy[x].GetComponent<NavMeshAgent>().speed = 0.8f;
        }    
        yield return new WaitForSeconds(timeFreze);

        for (int x = 0; x < countEnemy.Count; x++)
        {
            countEnemy[x].GetComponent<NavMeshAgent>().speed = 1.8f;
        }
        isFreze = false;
        yield break;
    } //Freze Active Buster

    public IEnumerator destroyBuster() //Destroy Active Buster
    {
        for (int x = 0; x < countEnemy.Count; x++)
        {
            countEnemy[x].GetComponent<EnemyAI>().DestroyEnemy();
            yield return new WaitForSeconds(.05f);
        }
        countEnemy.Clear();
        yield break;
    }
}
