using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    // UI
    [Header("UI Elements")]
    [SerializeField]
    private GameObject menuPanel;
    [SerializeField]
    private GameObject winPanel;
    [SerializeField]
    private GameObject losePanel;
    [SerializeField]
    private Slider gameSlider;
    [SerializeField]
    private TextMeshProUGUI txtCoinsPlayer;
    [SerializeField]
    private TextMeshProUGUI txtCountBustersFreze;
    [SerializeField]
    private TextMeshProUGUI txtCountBusstersDestroy;
    [SerializeField]
    private TextMeshProUGUI txtLevel;

    // GameAudio
    [Header("Audio Settings")]
    [SerializeField]
    private AudioClip click;
    [SerializeField]
    private AudioClip win;
    [SerializeField]
    private AudioClip lose;
    [SerializeField]
    private AudioClip kill;
    [SerializeField]
    private AudioClip giveItem;
    [SerializeField]
    private AudioClip frezeActive;
    [SerializeField]
    private AudioClip destroyActive;
    private AudioSource audioSource;

    [Header("RespawnScript")]
    public Respawn respawnScript;

    //Game_Manager
    private int levelGame;
    public int LevelGame
    {
        get { return levelGame; }
    }
    private bool isGame;
    public bool IsGame
    {
        get { return isGame; }
    }
    private bool isPaused;
    public bool IsPaused
    {
        get { return isPaused; }
    }

    private int coinsCount;
    private int countBusterFreze;
    private int countBusterDestroy;

    private List<GameObject> countDestroyEnemy = new List<GameObject>();
    public List<GameObject> CountDestroyEnemy
    {
        get { return countDestroyEnemy; }
        set { countDestroyEnemy = value; }
    }

    void Awake()
    {
        PreloadloadGame();
    }
    private void PreloadloadGame()
    {
        audioSource = GetComponent<AudioSource>();
        isGame = true;
        isPaused = false;

        losePanel.SetActive(false);
        winPanel.SetActive(false);
        menuPanel.SetActive(false);
        gameSlider.gameObject.SetActive(true);

        gameSlider.maxValue = 1;
        gameSlider.value = 0;

        if (PlayerPrefs.HasKey("Level"))
        {
            coinsCount = PlayerPrefs.GetInt("CoinsPlayer");
            levelGame = PlayerPrefs.GetInt("Level");
            countBusterFreze = PlayerPrefs.GetInt("BustersCountFreze");
            countBusterDestroy = PlayerPrefs.GetInt("BustersCountDestroy");
        }
        else
        {
            levelGame = 1;
            coinsCount = 0;
            countBusterFreze = 0;
            countBusterDestroy = 0;
        }

        txtLevel.text = $"Wave {levelGame}";
        txtCountBustersFreze.text = $"{countBusterFreze}";
        txtCountBusstersDestroy.text = $"{countBusterDestroy}";
        txtCoinsPlayer.text = $"{coinsCount}";
        StartCoroutine(sliderGame());
    } //Preload Game
    private void SaveGameResult()
    {
        PlayerPrefs.SetInt("Level", levelGame);
        PlayerPrefs.SetInt("CoinsPlayer", coinsCount);
        PlayerPrefs.SetInt("BustersCountFreze", countBusterFreze);
        PlayerPrefs.SetInt("BustersCountDestroy", countBusterDestroy);
        PlayerPrefs.SetFloat("RespawnTime", respawnScript.respawnTime);
    } //Save Game
    public void killEnemy()
    {
        audioSource.PlayOneShot(kill);
        int coinsRiward = Random.Range(25, 75);
        coinsCount += coinsRiward;
        txtCoinsPlayer.text = $"{coinsCount}";
    } //kill Enemy
    public void bustersGive(string namePotiton)
    {
        audioSource.PlayOneShot(giveItem);
        switch (namePotiton)
        {
            case "Freze":
                countBusterFreze++;
                txtCountBustersFreze.text = $"{countBusterFreze}";
                break;
            case "Bomb":
                countBusterDestroy++;
                txtCountBusstersDestroy.text = $"{countBusterDestroy}";
                break;
            default:
                break;
        }
    } //give Potiton
    public void UsePotitonFreze(float time)
    {
        if (countBusterFreze > 0)
        {
            audioSource.PlayOneShot(frezeActive);
            countBusterFreze--;
            txtCountBustersFreze.text = $"{countBusterFreze}";
            StartCoroutine(respawnScript.frezeBuster(time));
        }
    } //Use btnPotitonFreze
    public void UsePotitonDestroy()
    {
        if (countBusterDestroy > 0)
        {
            audioSource.PlayOneShot(destroyActive);
            countBusterDestroy--;
            txtCountBusstersDestroy.text = $"{countBusterDestroy}";
            StartCoroutine(respawnScript.destroyBuster());
        }
    } //Use btnPotitonDestroy
    public void Lose()
    {
        audioSource.PlayOneShot(lose);
        isGame = false;
        losePanel.SetActive(true);
    } //LoseGame
    public void Win()
    {
        audioSource.PlayOneShot(win);
        isGame = false;
        winPanel.SetActive(true);
    } //WinGame
    public void Menu()
    {
        audioSource.PlayOneShot(click);
        if (isGame)
        {
            if (!menuPanel.activeSelf)
            {
                isPaused = true;
                menuPanel.SetActive(true);
            }
            else
            {
                isPaused = false;
                menuPanel.SetActive(false);
            }
        }
    } //MenuPanel
    public void ExitGame()
    {
        SceneManager.LoadSceneAsync(0);
    } //Exit to menu
    public void RetartGame()
    {
        audioSource.PlayOneShot(click);
        SceneManager.LoadSceneAsync(1);
    } //Restart Game
    public void NextLevel() //Next Level
    {
        respawnScript.respawnTime -= 0.25f;
        levelGame++;

        audioSource.PlayOneShot(click);
        SaveGameResult();
        SceneManager.LoadSceneAsync(1);
    }
    private IEnumerator sliderGame()
    {
        while (gameSlider.value < 1 && isGame && !isPaused)
        {
            gameSlider.value += 0.03f * Time.deltaTime;
            yield return null;
        }

        if (isPaused)
        {
            while (isPaused)
            {
                yield return null;
            }
            StartCoroutine(sliderGame());
            yield break;
        }

        if (gameSlider.value >= 1 && isGame == true)
        {
            Win();
        }

        gameSlider.gameObject.SetActive(false);
        yield break;
    } // time Game
}
