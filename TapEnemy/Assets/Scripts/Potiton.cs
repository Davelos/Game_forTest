using System.Collections;
using UnityEngine;

public class Potiton : MonoBehaviour
{
    [SerializeField]
    private float timeLifeDrop;
    public enum pototonsName
    {
        Freze, Bomb
    }
    [SerializeField]
    private pototonsName potiton;

    private Respawn respawn;
    // Start is called before the first frame update
    void Start()
    {
        respawn = GetComponentInParent<Respawn>();
        StartCoroutine(lifeDrop());
    }

    private IEnumerator lifeDrop()
    {
        yield return new WaitForSeconds(timeLifeDrop);
        Destroy(this.gameObject);
        yield break;
    } //life Drop object

    private void OnMouseDown() //tap to Potiton
    {
        if (respawn.GameManager.IsGame)
        {
            respawn.GameManager.bustersGive(potiton.ToString());
            Destroy(this.gameObject);
        }
    }
}
