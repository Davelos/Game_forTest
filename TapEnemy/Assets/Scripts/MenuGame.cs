using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class MenuGame : MonoBehaviour
{
    [Header("Menu settings")]
    [SerializeField]
    private Button btnNewGame;
    [SerializeField]
    private Button btnRecords;
    [SerializeField]
    private Button btnExit;
    [SerializeField]
    private GameObject recordsPanel;
    [SerializeField]
    private TextMeshProUGUI txtRecords;
    [SerializeField]
    private GameObject titlesPanel;
    //Audio
    [SerializeField]
    private AudioClip click;
    private AudioSource audioSource;

    [Header("Title settings")]
    [SerializeField]
    private TextMeshProUGUI txtTittle;
    [SerializeField]
    private string[] txts;
    void Start()
    {
        preLoad();
    }

    private void preLoad() //preLoardGame
    {
        audioSource = GetComponent<AudioSource>();
        titlesPanel.SetActive(false);
        recordsPanel.SetActive(false);

        if (PlayerPrefs.HasKey("Level"))
        {
            btnNewGame.GetComponentInChildren<TextMeshProUGUI>().text = "Continue";
            btnRecords.interactable = true;
        }
        else
        {
            btnNewGame.GetComponentInChildren<TextMeshProUGUI>().text = "New Game";
            btnRecords.interactable = false;
        }
    }

    public void ExitGame() //ExitGame
    {
        audioSource.PlayOneShot(click);
        Application.Quit();
    }

    public void RecrodPanel() //RecordsClick
    {
        audioSource.PlayOneShot(click);

        if (!recordsPanel.activeSelf)
        {
            txtRecords.text = $"{PlayerPrefs.GetInt("CoinsPlayer")}";
            recordsPanel.SetActive(true);
           
        }
        else
        {
            recordsPanel.SetActive(false);
        }
    }

    public void PlayGame()//PlayGameClick
    {
        audioSource.PlayOneShot(click);
        SceneManager.LoadSceneAsync(1);
        btnNewGame.interactable = false;
    }

    public void titlesGame() //titlesClick
    {
        if (!titlesPanel.activeSelf)
        {
            titlesPanel.SetActive(true);
            StartCoroutine(titlesAction());
            btnRecords.interactable = false;
        }
        else
        {
            StopCoroutine(titlesAction());
            titlesPanel.SetActive(false);
            btnRecords.interactable = true;
        }
    }

    private IEnumerator titlesAction()//action Titles Game
    {
        for (int x = 0; x < txts.Length; x++)
        {
            txtTittle.text = $"{txts[x]}";
            yield return new WaitForSeconds(2f);
        }

        titlesPanel.SetActive(false);
        btnRecords.interactable = true;
        yield break;
    }
}
