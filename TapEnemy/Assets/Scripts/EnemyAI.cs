using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using TMPro;

public class EnemyAI : MonoBehaviour
{
    //Settings Enemy
    [Header("Enemy settings")]
    [SerializeField]
    private GameObject effectDestroy;
    [SerializeField]
    private TextMeshPro txtTapCoint;
    [SerializeField]
    private GameObject[] dropItems;

    private int coinsEXP;
    private Respawn respawn;
    private Animator animEnemy;
    private NavMeshAgent agent;
    private int tapCount;
    void Start()
    {
        preLoad();
        StartCoroutine(checkGameEnemy());
    }
    private void preLoad() //preload settings Enemy
    {
        animEnemy = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();

        if (PlayerPrefs.HasKey("Level"))
        {
            if (PlayerPrefs.GetInt("Level") > 1 && PlayerPrefs.GetInt("Level") <= 3)
            {
                agent.speed = Random.Range(1.6f, 2.6f);
                tapCount = Random.Range(1, 3);
            }
            else if (PlayerPrefs.GetInt("Level") > 3)
            {
                agent.speed = Random.Range(1.9f, 7.5f);
                tapCount = Random.Range(1, 4);
            }
;
        }
        else
        {
            tapCount = 1;
            agent.speed = 1.4f;
        }

        respawn = transform.parent.GetComponent<Respawn>();
        agent.SetDestination(respawn.TargetMove.position);
        animEnemy.SetBool("Walk", true);
        txtTapCoint.text = $"x{tapCount}";
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Children" && respawn.GameManager.IsGame)
        {
            animEnemy.SetBool("Victory", true);
            respawn.GameManager.Lose();
        }
    }

    private void OnMouseDown() //click to Enemy
    {
        if (respawn.GameManager.IsGame)
        {
            tapCount--;

            if (tapCount <= 0)
            {
                respawn.CountEnemy.Remove(this.transform.gameObject);
                respawn.GameManager.CountDestroyEnemy.Add(this.transform.gameObject);
                DestroyEnemy();
            }
            else
            {
                txtTapCoint.text = $"x{tapCount}";
            }
        }
    }

    public void DestroyEnemy() //Destroy Enemy
    {
        respawn.GameManager.killEnemy();

        if (respawn.GameManager.CountDestroyEnemy.Count >= 9)
        {
            respawn.GameManager.CountDestroyEnemy.Clear();
            DropItem();
        }

        Instantiate(effectDestroy, new Vector3(transform.position.x, transform.position.y + 0.6f, transform.position.z), Quaternion.identity);
        Destroy(this.gameObject);
    }

    private IEnumerator checkGameEnemy() //check Enemy game true/false
    {
        while (respawn.GameManager.IsGame && !respawn.GameManager.IsPaused)
        {
            yield return null;
        }

        this.agent.ResetPath();

        if (respawn.GameManager.IsPaused)
        {
            while (respawn.GameManager.IsPaused)
            {
                yield return null;
            }

            this.agent.SetDestination(respawn.TargetMove.position);
            StartCoroutine(checkGameEnemy());
            yield break;
        }

        yield break;
    }

    private void DropItem()
    {
        GameObject item = Instantiate(dropItems[Random.Range(0, dropItems.Length)], transform.parent);
        item.transform.position = this.transform.position;
    }
}
