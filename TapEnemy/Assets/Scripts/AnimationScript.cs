﻿using UnityEngine;

public class AnimationScript : MonoBehaviour 
{
    [SerializeField]
    private bool isAnimated = false;
    [SerializeField]
    private bool isRotating = false;
    [SerializeField]
    private Vector3 rotationAngle;
    [SerializeField]
    private float rotationSpeed;
 
	void Update () 
    {     
        if(isAnimated)
        {
            if(isRotating)
            {
                transform.Rotate(rotationAngle * rotationSpeed * Time.deltaTime); //Rotation Object
            }
        }
	}
}
